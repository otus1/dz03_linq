﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyLinq.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Middlename { get; set; }
        public string Phone { get; set; }
        public string Passport { get; set; }
        public DateTime CreationDate { get; set; }
        public string Login { get; set; }
        public string Password{ get; set; }
        public override string ToString()
        {
            return $"Id={Id}; Фамилия = {Surname}; Имя={Name}; Отчество={Middlename};";
        }
    }
}

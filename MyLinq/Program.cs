﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyLinq.Models;

namespace MyLinq
{
    public class Program
    {
        static public List<User> users;
        static public List<Account> accounts;
        static public List<History> histories;
        static void Initialize()
        {
            users = new List<User>();
            User user1 = new User() { Id = 1, CreationDate = DateTime.Now, Surname = "X1", Name = "X2", Middlename = "X3", Login = "1", Password = "1", Passport = "X_Pasport", Phone = "8-926-111-22-33" };
            User user2 = new User() { Id = 2, CreationDate = DateTime.Now, Surname = "Y1", Name = "Y2", Middlename = "Y3", Login = "2", Password = "2", Passport = "Y_Pasport", Phone = "8-926-222-33-44" };
            User user3 = new User() { Id = 3, CreationDate = DateTime.Now, Surname = "Z1", Name = "Z2", Middlename = "Z3", Login = "3", Password = "3", Passport = "Z_Pasport", Phone = "8-926-333-44-55" };
            users.Add(user1);
            users.Add(user2);
            users.Add(user3);

            accounts = new List<Account>();
            Account account1 = new Account() { Id = 1, CreationDate = DateTime.Now, Summ = 50, User = user1 };
            Account account2 = new Account() { Id = 2, CreationDate = DateTime.Now, Summ = 200, User = user2 };
            Account account3 = new Account() { Id = 3, CreationDate = DateTime.Now, Summ = 300, User = user3 };
            Account account4 = new Account() { Id = 4, CreationDate = DateTime.Now, Summ = 400, User = user1 };
            accounts.Add(account1);
            accounts.Add(account2);
            accounts.Add(account3);
            accounts.Add(account4);

            histories = new List<History>();
            histories.Add(new History() { Id = 1, Account = account1, CreationDate = DateTime.Now, Kind = 1, Summ = 100 });
            histories.Add(new History() { Id = 2, Account = account1, CreationDate = DateTime.Now, Kind = -1, Summ = -50 });
            histories.Add(new History() { Id = 3, Account = account2, CreationDate = DateTime.Now, Kind = 1, Summ = 200 });
            histories.Add(new History() { Id = 4, Account = account3, CreationDate = DateTime.Now, Kind = 1, Summ = 300 });
            histories.Add(new History() { Id = 5, Account = account4, CreationDate = DateTime.Now, Kind = 1, Summ = 400 });
        }

        static void UserInfo(string login, string password)
        {
            User u = users.Where(x => x.Login == "1" && x.Password == "1").FirstOrDefault();
            Console.WriteLine("user = " + u?.ToString());
        }
        //=================================================================
        static void AccountsForUser(User user)
        {
            foreach (var item in accounts.Where(x => x.User == user).ToList())
            {
                Console.WriteLine(item.ToString());
            }
        }

        //=================================================================
        static void AccountInfo(User user)
        {
            foreach (var a in accounts.Where(x => x.User == user).ToList())
            {
                Console.WriteLine(a.ToString());
                foreach (var h in histories.Where(x => x.Account == a).ToList())
                {
                    Console.WriteLine("    " + h.ToString());
                }
            }
        }
        //=================================================================
        static void HistoryPayAllUser()
        {
            var hh = from h in histories
                     join a in accounts on h.Account.Id equals a.Id
                     where h.Kind == 1
                     select (a.Id, a.Summ, a.CreationDate, a.User);


            foreach (var h in hh)
            {
                Console.WriteLine($"Id={h.Id}; Дата={h.CreationDate:d} Сумма={h.Summ} " + h.User.ToString());
            }

        }
        //=================================================================
        static void UserInfoForSumOverN()
        {
            Console.Write("Sum=");
            string stSum = Console.ReadLine();
            double flatSum = 0;
            try
            {
                flatSum = Convert.ToDouble(stSum);
            }
            catch
            { }

            foreach (var item in accounts.Where(x => x.Summ > flatSum).Select(x => x.User).Distinct())
            {
                Console.WriteLine(item.ToString());
            }
        }



        static void Main(string[] args)
        {
            Initialize();
            UserInfo("1", "1");

            Console.WriteLine("Account List:");
            AccountsForUser(users.Where(x => x.Id == 1).FirstOrDefault());

            //3 ===========================================================
            Console.WriteLine("");
            Console.WriteLine("Account Info:");
            AccountInfo(users.Where(x => x.Id == 1).FirstOrDefault());

            

            //4 ===========================================================
            Console.WriteLine("");
            Console.WriteLine("History pay:");
            HistoryPayAllUser();

            //5 ===========================================================
            Console.WriteLine("");
            Console.WriteLine("User summ > N");
            UserInfoForSumOverN();
            Console.ReadKey();
        }
    }
}
